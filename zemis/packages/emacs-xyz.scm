;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Jriga@zemis.co.uk
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (zemis packages emacs-xyz)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages emacs-xyz))


(define-public emacs-emacsql-next
  (package
      (name "emacs-emacsql-next")
      (version "64012261f65fcdd7ea137d1973ef051af1dced42")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/magit/emacsql")
               (commit version)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1x9r0pg2dv6n8dn1lwrvs9xfkxskr5pgw0sigspfqj3ycbpyz1ks"))))
      (build-system emacs-build-system)
      (propagated-inputs
       (list emacs-finalize))
      (home-page "https://github.com/magit/emacsql")
      (synopsis "Emacs high-level SQL database front-end")
      (description "Any readable Lisp value can be stored as a value in EmacSQL,
including numbers, strings, symbols, lists, vectors, and closures.  EmacSQL
has no concept of @code{TEXT} values; it's all just Lisp objects.  The Lisp
object @code{nil} corresponds 1:1 with @code{NULL} in the database. With emacsql-sqlite-common")
      (license license:expat)))

(define-public emacs-aide
  (package
    (name "emacs-aide")
    (version "a1b163f84a4d9bd5f67e2d887092f9b827f93336")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/junjizhi/aide.el.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0rx5g2hi5xskf605fa3pzi1a1ymg3vn7g73ixnk7q362z7r3rdhs"))))
    (build-system emacs-build-system)
    (propagated-inputs
     (list emacs-request))
    (home-page "https://github.com/junjizhi/aide.el")
    (synopsis "An Emacs front end for GPT APIs like OpenAI")
    (description "An Emacs front end for GPT APIs like OpenAI")
    (license license:expat)))

(define-public emacs-flycheck-vale
  (package
    (name "emacs-flycheck-vale")
    (version "7c7ebc3de058a321cb76348a01f45f02dc55d2f0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/abingham/flycheck-vale.git")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "086b2ljx3n2jpjm2vl7p0mnjbhx3v45kjrxd5y7q4ilhi29g5cpf"))))
    (build-system emacs-build-system)
    (propagated-inputs
     (list emacs-flycheck emacs-let-alist))
    (home-page "https://github.com/abingham/flycheck-vale")
    (synopsis "This package provides flycheck integration for vale")
    (description "This package provides flycheck integration for vale. Flycheck is an Emacs system for on-the-fly syntax checking. Vale is a natural language linter. So with flycheck-vale you get on-the-fly natural language linting.")
    (license license:expat)))


(define-public emacs-ask-it
  (package
   (name "emacs-ask-it")
   (version "caaf9211c6b8ebf4727eb734dbabd6cafdb4d909")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/jriga/askit")
           (commit version)))
     (file-name (git-file-name name version))
     (sha256
      (base32 "029a9k4bvhxz57y2xwaj4li466h0q0iishfz61d6d8z5pn97m0a0"))))
    (build-system emacs-build-system)
    (propagated-inputs '())
    (home-page "https://github.com/jriga/askit")
    (synopsis "Thin emacs wrapper for claude ai")
    (description "This package provides claude ai intergation with emacs and some org like ui")
    (license license:expat)))
