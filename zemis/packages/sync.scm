(define-module (zemis packages sync)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix utils)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression))

(define-public rclone
  (package
    (name "rclone")
    (version "1.69.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://downloads.rclone.org/v1.69.1/rclone-v" version "-linux-amd64.zip"))
       (sha256
        (base32 "176pwzmc7wckc8ba77fbqb1y5l2h7cdn0awkrb7yd6h2v3w42613"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("rclone" "bin/rclone"))))
    (inputs (list unzip))
    (synopsis "@code{rsync} for cloud storage")
    (description "@code{Rclone} is a command line program to sync files and
directories to and from different cloud storage providers.

Features include:
@itemize
@item MD5/SHA1 hashes checked at all times for file integrity
@item Timestamps preserved on files
@item Partial syncs supported on a whole file basis
@item Copy mode to just copy new/changed files
@item Sync (one way) mode to make a directory identical
@item Check mode to check for file hash equality
@item Can sync to and from network, e.g., two different cloud accounts
@item Optional encryption (Crypt)
@item Optional cache (Cache)
@item Optional FUSE mount (rclone mount)
@end itemize")
    (home-page "https://rclone.org/")
    (license license:expat)))
