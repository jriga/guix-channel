(define-module (zemis packages keymapp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system copy)
  #:use-module (guix utils)
  #:use-module (gnu packages base)
  #:use-module (gnu packages libusb))

(define-public keymapp
  (package
    (name "keymapp")
    (version "latest")
    (source (origin
              (method url-fetch)
              (uri "https://oryx.nyc3.cdn.digitaloceanspaces.com/keymapp/keymapp-latest.tar.gz")
              (sha256
               (base32 "11jf3n3akq5i4gixnf8xi3ymspav3zjrdp6lvccbl45nqxdzraxm"))))
    (build-system copy-build-system)
    (arguments
     `(#:validate-runpath? #f
       #:install-plan
       '(("keymapp" "bin/keymapp")
         ("icon.png" "share/icons/hicolor/256x256/apps/keymapp.png"))
       #:phases
       (modify-phases %standard-phases
         ; create a udev rules file for keymap /etc/udev/rules.d/50-zsa.rules
         (add-before 'install 'create-zsa-udev-rules-file
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (udev-rules-dir (string-append out "/lib/udev/rules.d")))
               (mkdir-p udev-rules-dir)
               (with-output-to-file (string-append udev-rules-dir "/50-zsa.rules")
                 (lambda _
                   (display "
# Rules for Oryx web flashing and live training
KERNEL==\"hidraw*\", ATTRS{idVendor}==\"16c0\", MODE=\"0664\", GROUP=\"plugdev\"
KERNEL==\"hidraw*\", ATTRS{idVendor}==\"3297\", MODE=\"0664\", GROUP=\"plugdev\"

# Legacy rules for live training over webusb (Not needed for firmware v21+)
  # Rule for all ZSA keyboards
  SUBSYSTEM==\"usb\", ATTR{idVendor}==\"3297\", GROUP=\"plugdev\"
  # Rule for the Moonlander
  SUBSYSTEM==\"usb\", ATTR{idVendor}==\"3297\", ATTR{idProduct}==\"1969\", GROUP=\"plugdev\"
  # Rule for the Ergodox EZ
  SUBSYSTEM==\"usb\", ATTR{idVendor}==\"feed\", ATTR{idProduct}==\"1307\", GROUP=\"plugdev\"
  # Rule for the Planck EZ
  SUBSYSTEM==\"usb\", ATTR{idVendor}==\"feed\", ATTR{idProduct}==\"6060\", GROUP=\"plugdev\"

# Wally Flashing rules for the Ergodox EZ
ATTRS{idVendor}==\"16c0\", ATTRS{idProduct}==\"04[789B]?\", ENV{ID_MM_DEVICE_IGNORE}=\"1\"
ATTRS{idVendor}==\"16c0\", ATTRS{idProduct}==\"04[789A]?\", ENV{MTP_NO_PROBE}=\"1\"
SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"16c0\", ATTRS{idProduct}==\"04[789ABCD]?\", MODE:=\"0666\"
KERNEL==\"ttyACM*\", ATTRS{idVendor}==\"16c0\", ATTRS{idProduct}==\"04[789B]?\", MODE:=\"0666\"

# Keymapp / Wally Flashing rules for the Moonlander and Planck EZ
SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"0483\", ATTRS{idProduct}==\"df11\", MODE:=\"0666\", SYMLINK+=\"stm32_dfu\"
# Keymapp Flashing rules for the Voyager
SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"3297\", MODE:=\"0666\", SYMLINK+=\"ignition_dfu\"
")))
               #t)))
         ; create .desktop file
         (add-after 'install 'create-desktop-file
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (applications-dir (string-append out "/share/applications"))
                    (bin-dir (string-append out "/bin")))
               (mkdir-p applications-dir)
               (with-output-to-file (string-append applications-dir "/keymapp.desktop")
                 (lambda _
                   (format #t
                           "[Desktop Entry]~@
                            Type=Application~@
                            Name=Keymapp~@
                            Exec=~a/keymapp~@
                            Icon=keymapp~@
                            Categories=Utility;~%"
                           bin-dir)))
               #t))))))
    (propagated-inputs (list libusb))
    (synopsis "Keymapp application")
    (description "Keymapp is an application for keyboard configuration.")
    (home-page "https://www.zsa.io/flash")
    (license license:expat)))

keymapp
