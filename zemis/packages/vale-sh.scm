;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2023 Jriga@zemis.co.uk
;;;
;;; This file is NOT part of GNU Guix.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (zemis packages vale-sh)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system copy))

(define-public vale-sh
  (package
   (name "vale-sh")
   (version "2.23.0")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
	   "https://github.com/errata-ai/vale/releases/download"
	   "/v" version
	   "/vale_" version "_Linux_64-bit.tar.gz"))
     (sha256
      (base32 "1y0d4dxszffc631d1g98xayrs9vmw0kvrgcmix25lyhp3hxf0b8g"))))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan '(("vale" "bin/vale"))))
   (home-page "https://vale.sh")
   (synopsis "Vale is an open-source, command-line tool that brings your editorial style guide to life")
   (description "Vale compiles into a fast, dependency-free binary for macOS, Windows, and Linux.
Vale helps you maintain a consistent and on-brand voice by allowing you to create your own custom rules.
Vale runs entirely offline—ensuring that your content is never sent to a remote server for processing.")
   (license license:expat)))
